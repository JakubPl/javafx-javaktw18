package pl.sda;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.TextInputDialog;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;
import javafx.stage.Window;

import java.util.Optional;
import java.util.Random;
import java.util.stream.IntStream;


public class Controller {
    public Button clearBtn;
    @FXML
    private Canvas canvas;
    @FXML
    private Button button;

    public void initialize() {
        final GraphicsContext gc = canvas.getGraphicsContext2D();

        drawRandomTriangles(gc, 1000);

        /*IntStream
                .range(0, 1000)
                .forEach(i -> drawRandomTriangle(gc));*/


        //x=50,y=50
        //x=280,y=100
        //x=400,y=200



        //gc.fillOval(100, 275, 25, 25);

        //gc.setFill(Color.RED);
        //gc.fillArc(100, 100, 100, 50, 180, 180, ArcType.OPEN);


       /* gc.setFill(Color.BLACK);
        gc.fillText("Rysowanie jest super.", 400, 500);
        double x = canvas.getWidth() / 2;
        double y = canvas.getHeight() / 2;
        gc.fillText("Pierwsza litera jest na (" + x + "," + y + ")", x, y);*/
    }

    private void drawRandomTriangles(GraphicsContext gc, int amountOfTriangles) {
        for(int i = 0; i < amountOfTriangles; i ++) {
            drawRandomTriangle(gc);
        }
    }

    private void drawRandomTriangle(GraphicsContext gc) {
        Random randomGen = new Random();
        Color rgb = randomColor();
        gc.setFill(rgb);
        int randomX1 = randomGen.nextInt((int) (canvas.getWidth() + 1));
        int randomX2 = randomGen.nextInt((int) (canvas.getWidth() + 1));
        int randomX3 = randomGen.nextInt((int) (canvas.getWidth() + 1));

        int randomY1 = randomGen.nextInt((int) (canvas.getHeight() + 1));
        int randomY2 = randomGen.nextInt((int) (canvas.getHeight() + 1));
        int randomY3 = randomGen.nextInt((int) (canvas.getHeight() + 1));

        double xs[] = {randomX1, randomX2, randomX3};
        double ys[] = {randomY1, randomY2, randomY3};
        gc.fillPolygon(xs, ys, 3);
    }

    private Color randomColor() {
        Random randomGen = new Random();
        int blue = randomGen.nextInt(256);
        int green = randomGen.nextInt(256);
        int red = randomGen.nextInt(256);
        return Color.rgb(red, green, blue);
    }

    public void onClearBtnClicked(MouseEvent mouseEvent) {
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setFill(Color.BLACK);
        gc.fillRect(0,0,canvas.getWidth(), canvas.getHeight());
    }

    public void onStartBtnClicked(MouseEvent mouseEvent) {
        GraphicsContext gc = canvas.getGraphicsContext2D();
        Window owner = canvas.getScene().getWindow();
        TextInputDialog alert = new TextInputDialog();
        alert.setTitle("Trojkaty");
        alert.setContentText("Wpisz liczbe trojkatow");
        alert.initOwner(owner);
        final Optional<String> result = alert.showAndWait();
        result.ifPresent(res -> {
            int amountOfTrianglesToDraw = Integer.parseInt(res);
            drawRandomTriangles(gc, amountOfTrianglesToDraw);
        });
    }
}
